---
layout: default
permalink: /
---

<div class="flex">
  <img src="{% asset jared.jpg @path %}" class="profile-image" />
  <ul>
    <li>i’m a software engineer based in brooklyn, new york</li>
    <li>i work at <a href="https://www.figma.com" target="_blank">figma</a></li>
    <li>i used to work at <a href="https://join.mobilize.us" target="_blank">mobilize</a>, and before that, the <a href="https://www.nytimes.com" target="_blank">new york times</a></li>
    <li>i’m also a proud alumnus of the <a href="https://recurse.com/" target="_blank">recurse center</a>, a self-directed educational retreat for programmers</li>
  </ul>
</div>

<hr />

selected internet presence:

- 👨‍💻 [github](https://github.com/jaredmcdonald)
- 👨‍💼 [linkedin](https://linkedin.com/in/jaredmcd)

<hr />

get in touch (no recruiters, please):

jared [dot] r [dot] mcdonald [at] gmail [dot] com
